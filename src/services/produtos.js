const { api } = require('./api')

export default {
	listar: () => {
		return api.get('/products?limit=50')
	},
	mostrar: (id) => {
		return api.get('/products/' + id)
	},
	criar: (produto) => {
		return api.post('/products', produto)
	},
	editar: (id, produto) => {
		return api.put('/products/' + id, produto)
	},
	deletar: (id) => {
		return api.delete('/products/' + id)
	}
}